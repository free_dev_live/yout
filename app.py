from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template("q.html")


@app.route('/inline')
def inline():
    return render_template('inline.html')


if __name__ == '__main__':
    app.run()
